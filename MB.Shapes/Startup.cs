using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MB.Shapes.DataAccess;
using MB.Shapes.Core;
using Microsoft.EntityFrameworkCore;

namespace MB.Shapes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<ShapeService>();
            services.AddScoped<CalculateShapeAreaByIdQuery>();
            services.AddDbContext<ShapeContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("ShapeContext")));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ShapeContext dataContext)
        {
            dataContext.Database.EnsureCreated();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
