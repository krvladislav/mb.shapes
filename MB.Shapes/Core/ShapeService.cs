using System;
using MB.Shapes.DataAccess;
using Newtonsoft.Json;

namespace MB.Shapes.Core
{
    public class ShapeService
    {
        private readonly ShapeContext _context;

        public ShapeService(ShapeContext context)
        {
            _context = context;
        }

        public Circle CreateCircle(double radius)
        {
            var id = Guid.NewGuid();
            var circle = new Circle(id, radius);

            var dao = new CircledDAO { Radius = circle.Radius };
            var entity = _context.Add(new ShapeDAO { 
                Id = id,
                Descriptor = JsonConvert.SerializeObject(dao),
                TypeName = dao.GetType().FullName,
                Area = circle.CalculateArea()
            });
            _context.SaveChanges();

            return circle;
        }

        public Triangle CreateTriangle(double a, double b, double c)
        {
            var id = Guid.NewGuid();
            var triangle = new Triangle(id, a, b, c);

            var dao = new TriangleDAO { A = triangle.A, B = triangle.B, C = triangle.C };
            _context.Add(new ShapeDAO { 
                Id = id, 
                Descriptor = JsonConvert.SerializeObject(dao),
                TypeName = dao.GetType().FullName,
                Area = triangle.CalculateArea()
            });
            _context.SaveChanges();
            
            return triangle;
        }
    }
}