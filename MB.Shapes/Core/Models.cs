using System;

namespace MB.Shapes
{
    public abstract class Shape
    {
        public abstract double CalculateArea();
    }

    public class Circle : Shape
    {
        public Guid Id { get; private set; }
        public double Radius { get; private set; }

        public Circle(Guid id, double radius)
        {
            if (radius < double.Epsilon) { throw new ArgumentException("must be positive", nameof(radius)); }
            
            Id = id;
            Radius = radius;
        }
        
        public override double CalculateArea()  
        { 
            return Math.PI * Radius * Radius;
        }
    }

    public class Triangle : Shape
    {
        public Guid Id { get; private set; }
        public double A { get; private set; }
        public double B { get; private set; }
        public double C { get; private set; }

        public Triangle(Guid id, double a, double b, double c)
        {
            if (a < double.Epsilon) { throw new ArgumentException("must be positive", nameof(a)); }
            if (b < double.Epsilon) { throw new ArgumentException("must be positive", nameof(b)); }
            if (c < double.Epsilon) { throw new ArgumentException("must be positive", nameof(c)); }

            Id = id;
            A = a;
            B = b;
            C = c;
        }

        public override double CalculateArea()  
        { 
            var s = (A + B + C) / 2;
            var S = Math.Sqrt(s * (s - A) * (s - B) * (s - C));
            return S;
        }
    }
}