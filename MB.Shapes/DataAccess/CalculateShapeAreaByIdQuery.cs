using System;

namespace MB.Shapes.DataAccess
{
    public class CalculateShapeAreaByIdQuery
    {
        private readonly ShapeContext _context;

        public CalculateShapeAreaByIdQuery(ShapeContext context)
        {
            _context = context;
        }

        public double? Exec(Guid id)
        {
            var shape = _context.Shapes.Find(id);
            if (shape == null) return null;
            return shape.Area;
        }
    }
}