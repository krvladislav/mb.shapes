using System;

namespace MB.Shapes.DataAccess
{
    public class ShapeDAO
    {
        public Guid Id { get; set; }
        public string Descriptor { get; set; }
        public string TypeName { get; set; }
        public double Area { get; set;}
    }

    public class CircledDAO
    {
        public double Radius { get; set; }
    }

    public class TriangleDAO
    {        
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
    }
}
