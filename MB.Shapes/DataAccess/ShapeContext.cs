using Microsoft.EntityFrameworkCore;

namespace MB.Shapes.DataAccess
{
    public class ShapeContext : DbContext
    {   
        public ShapeContext (DbContextOptions<ShapeContext> options) : base(options)
        {
        }

        public DbSet<ShapeDAO> Shapes { get; set; }
    }
}