﻿using System;
using Microsoft.AspNetCore.Mvc;
using MB.Shapes.Core;
using MB.Shapes.DataAccess;

namespace MB.Shapes.API
{
    [ApiController]
    [Route("shape")]
    public class ShapeController : ControllerBase
    {
        private readonly ShapeService _shapeService;
        private readonly CalculateShapeAreaByIdQuery _calculateShapeAreaByIdQuery;

        public ShapeController(
            ShapeService shapeService, 
            CalculateShapeAreaByIdQuery calculateShapeAreaByIdQuery)
        {
            _shapeService = shapeService;
            _calculateShapeAreaByIdQuery = calculateShapeAreaByIdQuery;
        }

        [HttpPost]
        [Route("circle")]
        public IActionResult CreateCircle([FromBody] CircleRequest req)
        {
            if (req.Radius < double.Epsilon) { 
                return BadRequest("must be positive: " + nameof(req.Radius)); 
            }
            
            var circle = _shapeService.CreateCircle(req.Radius);
            return Ok(circle.Id);
        }

        [HttpPost]
        [Route("triangle")]
        public IActionResult CreateTriangle([FromBody] TriangleRequest req)
        {
            if (req.A < double.Epsilon) 
            { 
                return BadRequest("must be positive: " + nameof(req.A));  
            }
            if (req.B < double.Epsilon) 
            { 
                return BadRequest("must be positive: " + nameof(req.B));  
            }
            if (req.C < double.Epsilon) 
            { 
                return BadRequest("must be positive: " + nameof(req.C)); 
            }

            var triangle = _shapeService.CreateTriangle(req.A, req.B, req.C);
            return Ok(triangle.Id);
        }
        
        [HttpGet]
        [Route("{id}/area")]
        public IActionResult CalculateArea(Guid id)
        {
            var area = _calculateShapeAreaByIdQuery.Exec(id);
            if (area == null)
            {
                return NotFound();
            }
            return Ok(area);
        }
    }
}
