using System;

namespace MB.Shapes.API
{
    public class CircleRequest
    {
        public double Radius { get; set;}
    }

    public class TriangleRequest
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
    }
}