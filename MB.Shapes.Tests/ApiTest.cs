using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using MB.Shapes.DataAccess;

namespace Tests
{
    public class ApiTest
    {
        private readonly HttpClient _apiClient;
        private readonly ShapeContext _dataContext;

        public ApiTest()
        {
            var factory = new WebApplicationFactory<MB.Shapes.Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.UseSetting("Environment", "Testing");
                });

            _dataContext = (ShapeContext)factory.Services.GetService(typeof(ShapeContext));
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            _apiClient = factory.CreateClient();
        }

        [Fact]
        public async Task AreaOfCircleRadius1_ShouldBeEqualToPI()
        {
            var circleResponse = await _apiClient.PostAsJsonAsync("/shape/circle", new
            {
                Radius = 1.0
            });
            circleResponse.EnsureSuccessStatusCode();   

            var circleId = (await circleResponse.Content.ReadAsStringAsync()).Replace("\"","");
            var areaResponse = await _apiClient.GetAsync($"/shape/{circleId}/area");
            areaResponse.EnsureSuccessStatusCode();

            var area = await areaResponse.Content.ReadAsStringAsync();  
            Assert.Equal(Math.PI, double.Parse(area), 5);
        }

        [Fact]
        public async Task AreaOfTrangle_ShouldBeEqualTo05()
        {
            var triangleResponse = await _apiClient.PostAsJsonAsync("/shape/triangle", new
            {
                A = 1.0,
                B = 1.0,
                C = Math.Sqrt(2.0)
            });
            triangleResponse.EnsureSuccessStatusCode();   

            var triangleId = (await triangleResponse.Content.ReadAsStringAsync()).Replace("\"","");
            var areaResponse = await _apiClient.GetAsync($"/shape/{triangleId}/area");
            areaResponse.EnsureSuccessStatusCode();

            var area = await areaResponse.Content.ReadAsStringAsync();  
            Assert.Equal(0.5, double.Parse(area), 5);
        }
        
        [Fact]
        public async Task StatusCodeForNonExistingShape_ShouldBeNotFound()
        {
            var areaResponse = await _apiClient.GetAsync($"/shape/{Guid.Empty}/area");
            
            Assert.Equal(HttpStatusCode.NotFound, areaResponse.StatusCode);
        }

        [Fact]
        public async Task ShapeCreationWithNegativeValues_ShouldReturnBadRequest1()
        {
            var circleResponse = await _apiClient.PostAsJsonAsync("/shape/circle", new
            {
                Radius = -1.0
            });
            Assert.Equal(HttpStatusCode.BadRequest, circleResponse.StatusCode);
        }
        
        [Fact]
        public async Task ShapeCreationWithNegativeValues_ShouldReturnBadRequest2()
        {
            var triangleResponse = await _apiClient.PostAsJsonAsync("/shape/triangle", new
            {
                A = -1.0,
                B = 1.0,
                C = 1.0
            });
            Assert.Equal(HttpStatusCode.BadRequest, triangleResponse.StatusCode);
        }

        [Fact]
        public async Task ShapeCreationWithNegativeValues_ShouldReturnBadRequest3()
        {
            var triangleResponse = await _apiClient.PostAsJsonAsync("/shape/triangle", new
            {
                A = 1.0,
                B = -1.0,
                C = 1.0
            });
            Assert.Equal(HttpStatusCode.BadRequest, triangleResponse.StatusCode);
        }

        [Fact]
        public async Task ShapeCreationWithNegativeValues_ShouldReturnBadRequest4()
        {
            var triangleResponse = await _apiClient.PostAsJsonAsync("/shape/triangle", new
            {
                A = 1.0,
                B = 1.0,
                C = -1.0
            });
            Assert.Equal(HttpStatusCode.BadRequest, triangleResponse.StatusCode);
        }
         
    }
}
